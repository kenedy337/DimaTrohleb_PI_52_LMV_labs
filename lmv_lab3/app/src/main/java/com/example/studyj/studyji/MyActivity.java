package com.example.studyj.studyji;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;

public class MyActivity extends AppCompatActivity {


    SQLiteDatabase db;
    private EditText fio;
    private EditText service;
    private EditText data;
    private EditText price;
    private Button button1;
    Cursor userCursor;
    SimpleCursorAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fio = (EditText) findViewById(R.id.fio);
        service = (EditText) findViewById(R.id.service);
        data = (EditText) findViewById(R.id.data);
        price = (EditText) findViewById(R.id.price);

        button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                db.execSQL("INSERT INTO users " +
                        "(fio, service, data, price)" +
                        " VALUES (" +
                        "'" + fio.getText().toString() + "'," +
                        "'" + service.getText().toString() + "'," +
                        "'" + data.getText().toString() + "'," +
                        "'" + price.getText().toString() + "'" +
                        ");");
                clearInputs();
            }
        });

    }

    @Override
    public void onResume(){
        super.onResume();
        try {

            db = getBaseContext().openOrCreateDatabase("app.db", MODE_PRIVATE, null);
            db.execSQL("CREATE TABLE IF NOT EXISTS users (_id INTEGER PRIMARY KEY AUTOINCREMENT,fio TEXT, service TEXT, data TEXT, price TEXT);");
        }catch (SQLiteException excp) {
            int a = 2;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void addNew() {


    }

    public void onClick(View view) {
        Intent intent = new Intent(MyActivity.this, newactivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        // Закрываем подключения
        db.close();

    }
    protected void clearInputs() {
        fio.setText("");
        service.setText("");
        data.setText("");
        price.setText("");
    }

}
