package com.example.studyj.studyji;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


public class newactivity extends AppCompatActivity {

    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newlayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = getBaseContext().openOrCreateDatabase("app.db", MODE_PRIVATE, null);
        ListView list1 = (ListView)findViewById(R.id.list);
        render();
    }

    public void render(){
        Cursor userCursor =  db.rawQuery("select * from users", null);
        String[] headers = new String[] {"fio", "service", "data", "price"};
        SimpleCursorAdapter userAdapter = new SimpleCursorAdapter(this, R.layout.custom_list_items,
                userCursor, headers, new int[]{R.id.text0, R.id.text2, R.id.text3, R.id.text4}, 0);
        ListView list1 = (ListView)findViewById(R.id.list);
        list1.setAdapter(userAdapter);
    }

    public void onClick(View view) {
        Intent intent = new Intent(newactivity.this, MyActivity.class);
        startActivity(intent);
    }

}